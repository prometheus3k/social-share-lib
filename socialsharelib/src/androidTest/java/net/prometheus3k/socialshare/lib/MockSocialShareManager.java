package net.prometheus3k.socialshare.lib;

/**
 * Created by babumadhikarmi on 10/05/2015.
 */
public class MockSocialShareManager extends SocialShareManager {

    public void addMockNetwork(SocialNetwork network){
        mSocialNetworkClients.add(network);
    }
}
