package net.prometheus3k.socialshare.lib;

import android.net.Uri;
import android.test.AndroidTestCase;

/**
 * Created by babumadhikarmi on 09/05/2015.
 */
public class SocialNetworkTestCase extends AndroidTestCase{

    /**
     * Test the "social network app installed" logic.
     */
    public void testInstallDetection(){

        String packageName = getContext().getPackageName();
        assertTrue(packageName.equals(Constants.INSTALLED_PACKAGE));

        SocialNetwork installedClient = new MockSocialNetwork(getContext(), packageName, false, false);
        assertTrue(installedClient.isInstalled());

        SocialNetwork notInstalledClient = new MockSocialNetwork(getContext(), "com.twitter.blah", false, false);
        assertFalse(notInstalledClient.isInstalled());
    }

    public void testCanShare(){
        //create a social network that supports image only
        SocialNetwork network = new MockSocialNetwork(getContext(), Constants.INSTALLED_PACKAGE, false, true);

        //create/share a text only share item: fail
        ShareItem textShare = new ShareItem("hello");
        assertFalse(network.canShare(textShare));

        //create/share a image only share item: pass
        Uri.Builder builder = new Uri.Builder();
        ShareItem imageShare = new ShareItem(builder.build());
        assertTrue(network.canShare(imageShare));
    }

    public void testValidation(){
        //create a social network that supports text/image
        MockSocialNetwork network = new MockSocialNetwork(getContext(), Constants.INSTALLED_PACKAGE, true, true);

        boolean didThrow = false;
        //create an empty share item and validate: fail
        try {
            network.validate(new ShareItem(null, null));
        } catch(RuntimeException e){
            e.printStackTrace();
            didThrow = true;
        }
        assertTrue(didThrow);
    }
}
