package net.prometheus3k.socialshare.lib;

import android.test.AndroidTestCase;

import java.util.List;

/**
 * Created by babumadhikarmi on 10/05/2015.
 */
public class SocialShareManagerTestCase extends AndroidTestCase{

    public void testNoCompatibleNetwork(){
        ShareItem textShareItem = new ShareItem("test");

        //Mock SocialHelper that has empty network clients list
        MockSocialShareManager helper = new MockSocialShareManager();

        //add a mock twitter app that does not support text or image
        helper.addMockNetwork(new MockSocialNetwork(getContext(), "blah.debug", false, false));

        //Test: do we have a compatible network for text share item?
        List<SocialNetwork> compatibleNetworks = helper.getCompatibleNetworks(textShareItem, SocialNetwork.Platform.Twitter);
        assertEquals(0, compatibleNetworks.size());

        //add a mock twitter app that supports text only
        helper.addMockNetwork(new MockSocialNetwork(getContext(), Constants.INSTALLED_PACKAGE, true, false));

        //Test: we should have one network that supports text sharing
        compatibleNetworks = helper.getCompatibleNetworks(textShareItem, SocialNetwork.Platform.Any);
        assertEquals(1, compatibleNetworks.size());
    }

    public void testShareErrorWithNoCompatibleNetworks(){
        //Mock SocialHelper that has empty network clients list
        MockSocialShareManager helper = new MockSocialShareManager();
        //add a mock twitter app that supports no sharing
        helper.addMockNetwork(new MockSocialNetwork(getContext(), "blah.debug", false, false));

        //add a mock twitter app that supports image sharing only
        helper.addMockNetwork(new MockSocialNetwork(getContext(), "blah.debug", false, true));

        //if we try to share with no compatible networks, we get an error object.
        Error error = helper.share(new ShareItem("test"), SocialNetwork.Platform.Twitter);
        assertNotNull(error);
    }
}
