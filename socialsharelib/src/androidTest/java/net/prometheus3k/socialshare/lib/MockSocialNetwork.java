package net.prometheus3k.socialshare.lib;

import android.content.Context;

/**
 * Created by babumadhikarmi on 10/05/2015.
 */
public class MockSocialNetwork extends SocialNetwork {
    private boolean mCanShareText;
    private boolean mCanShareImage;

    public MockSocialNetwork(Context context, String packageName, boolean canShareText, boolean canShareImage) {
        super(context, packageName, Platform.Twitter);
        mCanShareText = canShareText;
        mCanShareImage = canShareImage;
    }

    @Override
    protected boolean canShareText() {
        return mCanShareText;
    }

    @Override
    protected boolean canShareImage() {
        return mCanShareImage;
    }

    @Override
    public void share(ShareItem shareItem) {

    }

    @Override
    public void validate(ShareItem shareItem) {
        super.validate(shareItem);
    }
}
