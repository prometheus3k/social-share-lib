/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.socialshare.lib.socialnetworks;

import android.content.Context;

import net.prometheus3k.socialshare.lib.IntentBasedSocialNetwork;

/**
 * Represents a single Google plus client
 * Created by babumadhikarmi on 09/05/2015.
 */
public class GooglePlus extends IntentBasedSocialNetwork {
//    private static final String TAG = GooglePlus.class.getSimpleName();

    public GooglePlus(Context context){
        super(context, "com.google.android.apps.plus", Platform.GooglePlus);
    }

    @Override
    protected boolean canShareText(){
        return true;
    }
    
    protected boolean canShareImage(){
        return true;
    }
}
