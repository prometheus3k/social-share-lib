/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.socialshare.lib.socialnetworks;

import android.content.Context;

import net.prometheus3k.socialshare.lib.ShareItem;
import net.prometheus3k.socialshare.lib.WebBasedSocialNetwork;

/**
 * Created by babumadhikarmi on 09/05/2015.
 */
public class TwitterWeb extends WebBasedSocialNetwork {
//    private static final String TAG = TwitterWeb.class.getSimpleName();

    protected static final String TWEET_URL = "https://twitter.com/intent/tweet?source=webclient";
    protected static final String TWEET_TEXT_SUFFIX = "&text=%s";

    public TwitterWeb(Context context, String packageName) {
        super(context, packageName, Platform.Twitter);
    }

    @Override
    protected boolean canShareText() {
        return true;
    }

    @Override
    protected boolean canShareImage() {
        return false;
    }

    @Override
    protected String getShareUrl(ShareItem shareItem) {
        return String.format(TWEET_URL + TWEET_TEXT_SUFFIX, urlEncode(shareItem.getText()));
    }
}
