/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.socialshare.lib.socialnetworks;

import android.content.Context;

import net.prometheus3k.socialshare.lib.IntentBasedSocialNetwork;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by babumadhikarmi on 09/05/2015.
 */
public class TwitterApp extends IntentBasedSocialNetwork {
//    private static final String TAG = TwitterApp.class.getSimpleName();

    private static ArrayList<String> sSupportedAppClients = new ArrayList<>(
            Arrays.asList(
                    "com.twitter.android",    //official twitter app
                    "com.echofon",
                    "com.levelup.touiteur",
                    "com.seesmic",
                    "com.handlerexploit.tweedle",
                    "com.hootsuite.droid.full"
            )
    );

    public TwitterApp(Context context, String packageName) {
        super(context, packageName, Platform.Twitter);
    }

    protected boolean canShareText(){
        return true;
    }

    protected boolean canShareImage(){
        return true;
    }

    public static List<TwitterApp> getClients(Context context){
        List<TwitterApp> clients = new ArrayList<>();
        for(String packageName : sSupportedAppClients){
            clients.add(new TwitterApp(context, packageName));
        }
        return clients;
    }
}
