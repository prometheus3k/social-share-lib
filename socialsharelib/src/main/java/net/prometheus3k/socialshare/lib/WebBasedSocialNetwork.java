/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.socialshare.lib;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;

import net.prometheus3k.socialshare.lib.socialnetworks.TwitterWeb;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by babumadhikarmi on 17/06/2015.
 */
public abstract class WebBasedSocialNetwork extends SocialNetwork{
    private static final String TAG = WebBasedSocialNetwork.class.getSimpleName();

    public WebBasedSocialNetwork(Context context, String packageName, Platform platform) {
        super(context, packageName, platform);
    }

    //depends on social network
    protected abstract String getShareUrl(ShareItem shareItem);

    @Override
    public void share(ShareItem shareItem) {
        final String shareUrl = getShareUrl(shareItem);

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(shareUrl));
        intent.setPackage(getMetaData().getPackageName());
        mContext.startActivity(intent);
    }

    public static List<TwitterWeb> getClients(Context context){
        List<TwitterWeb> twitterWebClients = new ArrayList<>();

        //an intent only browsers would handle.
        Intent queryIntent = new Intent(Intent.ACTION_VIEW);
        queryIntent.setData(Uri.parse("http://www.google.co.uk"));

        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(queryIntent, 0);
        if (!resInfo.isEmpty()){
            for (ResolveInfo info : resInfo) {
                String packageName = info.activityInfo.packageName;
                twitterWebClients.add(new TwitterWeb(context, packageName));
            }
        }
        return twitterWebClients;
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.wtf(TAG, "UTF-8 should always be supported", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }
}
