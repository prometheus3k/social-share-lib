/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.socialshare.lib;

import android.net.Uri;
import android.util.Log;

import java.io.File;

/**
 * Created by babumadhikarmi on 09/05/2015.
 */
public class ShareItem {
    public static final String TAG = ShareItem.class.getSimpleName();
    
    private String mText;
    private Uri mImagePath;

    public ShareItem(String text){
        mText = text;
    }

    public ShareItem(Uri imagePath){
        mImagePath = imagePath;
    }

    public ShareItem(String text, Uri imagePath){
        this(text);

        if(new File(imagePath.getPath()).exists()){
            mImagePath = imagePath;
        } else {
            Log.e(TAG, "FILE NOT FOUND! " + mImagePath);
        }
    }

    public String getText(){
        return mText;
    }

    public String getImagePath(){
        if(mImagePath == null){
            return null;
        }
        return mImagePath.getPath();
    }

    public boolean isEmpty(){
        return mText == null && mImagePath == null;
    }

    @Override
    public String toString() {
        return TAG + "{ text: " + mText + ", imagePath: " + mImagePath.getPath() + "}";
    }
}
