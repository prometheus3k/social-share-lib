/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.socialshare.lib.socialnetworks;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.widget.Toast;

import net.prometheus3k.socialshare.lib.IntentBasedSocialNetwork;
import net.prometheus3k.socialshare.lib.R;
import net.prometheus3k.socialshare.lib.ShareItem;

/**
 * Represents a single Facebook client
 * Created by babumadhikarmi on 18/05/2015.
 */
public class FacebookAppIntent extends IntentBasedSocialNetwork {
//    private static final String TAG = FacebookAppIntent.class.getSimpleName();

    public FacebookAppIntent(Context context){
        super(context, "com.facebook.katana", Platform.Facebook);
    }

    @Override
    protected boolean canShareText(){
        return true;
    }

    protected boolean canShareImage(){
        return true;
    }

    @Override
    public void share(ShareItem shareItem) {
        super.share(shareItem);
        // Note: the facebook app ignores the Intent.EXTRA_TEXT field in the Intent.
        // The closest we can get is to copy the text to clipboard and tell the user to paste it in.
        // So if ShareItem has text, copy to clipboard and display toast.
        if(shareItem.getText() != null && !shareItem.getText().isEmpty()) {
            ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setPrimaryClip(ClipData.newPlainText("status_message", shareItem.getText()));
            Toast.makeText(mContext, mContext.getString(R.string.paste_status_message), Toast.LENGTH_LONG).show();
        }
    }
}
