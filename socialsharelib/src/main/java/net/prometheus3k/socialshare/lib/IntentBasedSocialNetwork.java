/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.socialshare.lib;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.io.File;

/**
 * Created by babumadhikarmi on 17/06/2015.
 */
public abstract class IntentBasedSocialNetwork extends SocialNetwork{
    private static final String TAG = IntentBasedSocialNetwork.class.getSimpleName();

    public IntentBasedSocialNetwork(Context context, String packageName, Platform platform) {
        super(context, packageName, platform);
    }


    @Override
    public void share(ShareItem shareItem) {
        validate(shareItem);

        Intent targetedShare = new Intent(android.content.Intent.ACTION_SEND);
        targetedShare.setPackage(getMetaData().getPackageName());
        if(shareItem.getText() != null) {
            targetedShare.putExtra(Intent.EXTRA_TEXT, shareItem.getText());
            //share text only
            targetedShare.setType("text/plain"); // put here your mime type
        }

        if(shareItem.getImagePath() != null){
            //share image (possibly with text)
            File imageFile = new File(shareItem.getImagePath());
            Uri imageUri = Uri.fromFile(imageFile);
            targetedShare.setType("image/*"); // put here your mime type
            targetedShare.putExtra(Intent.EXTRA_STREAM, imageUri);
        }

        mContext.startActivity(targetedShare);
    }
}
