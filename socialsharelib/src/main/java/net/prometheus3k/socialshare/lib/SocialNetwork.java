/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.socialshare.lib;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.Log;

/**
 * Created by babumadhikarmi on 09/05/2015.
 */
public abstract class SocialNetwork {
    public enum Platform {
        Any,
        Twitter,
        GooglePlus,
        Facebook
    }

    private static final String TAG = SocialNetwork.class.getSimpleName();
    public class MetaData{
        private String mName;
        private Drawable mIcon;
        private String mPackageName;

        public MetaData(String name, Drawable icon){
            mName = name;
            mIcon = icon;
        }

        public MetaData(String packageName, String name, Drawable icon){
            this(name, icon);
            mPackageName = packageName;
        }

        public String getName(){
            return mName;
        }

        public Drawable getIcon(){
            return mIcon;
        }

        public String getPackageName(){
            return mPackageName;
        }

        @Override
        public String toString() {
            return "name: " + mName + ", package: " + mPackageName;
        }
    }

    //TODO url and hashtag support

    protected MetaData mMetaData;
    protected final Context mContext;

    private final Platform mPlatform;

    public SocialNetwork(Context context, String packageName, Platform platform){
        mPlatform = platform;
        mContext = context;
        try {
            ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(packageName, 0);
            String appName = context.getPackageManager().getApplicationLabel(appInfo).toString();
            Drawable appIcon = context.getPackageManager().getApplicationIcon(appInfo);
            mMetaData = new MetaData(packageName, appName, appIcon);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "No package found " + packageName);
        }
    }

    public MetaData getMetaData(){
        return mMetaData;
    }


    public Platform getNetworkPlatform() {
        return mPlatform;
    }

    boolean canShare(ShareItem shareItem){
        if(shareItem.getText() != null && shareItem.getImagePath() != null){
           //can share both
            return canShareText() && canShareImage();

        } else if(shareItem.getText() != null){
            //can share text
            return canShareText();

        } else if(shareItem.getImagePath() != null){
            //can share image
            return canShareImage();
        }
        return false;
    }

    protected boolean canShareText(){
        return false;
    }

    protected boolean canShareImage(){
        return false;
    }

    boolean isInstalled() {
        PackageManager pm = mContext.getPackageManager();
        try {

            if(getMetaData() == null){
                return false;
            }
            String packageName = getMetaData().getPackageName();
            pm.getPackageInfo(packageName, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    protected void validate(ShareItem shareItem){
        if(!canShare(shareItem)){
            throw new RuntimeException(TAG + " does not support sharing of this item!");
        }

        if(shareItem.isEmpty()) {
            throw new RuntimeException("Unable to share empty ShareItem!");
        }
    }

    public abstract void share(ShareItem shareItem);

    @Override
    public String toString() {
        return mPlatform.toString() + " - " + (mMetaData == null ? "No MetaData" : mMetaData.toString());
    }


}
