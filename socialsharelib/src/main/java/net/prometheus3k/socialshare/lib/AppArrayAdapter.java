package net.prometheus3k.socialshare.lib;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by babumadhikarmi on 10/05/2015.
 */
public class AppArrayAdapter extends ArrayAdapter<SocialNetwork> {

    public AppArrayAdapter(Context context, List<SocialNetwork> objects) {
        super(context, R.layout.share_with_row, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        SocialNetwork.MetaData metaData = getItem(position).getMetaData();

        //get viewHolder
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.share_with_row, null);

            viewHolder = new ViewHolder();
            viewHolder.mImageView = (ImageView)convertView.findViewById(R.id.icon);
            viewHolder.mTextView = (TextView)convertView.findViewById(R.id.title);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //Set data
        viewHolder.mImageView.setImageDrawable(metaData.getIcon());
        viewHolder.mTextView.setText(metaData.getName());
        return convertView;
    }

    private class ViewHolder {
        ImageView mImageView;
        TextView mTextView;
    }
}
