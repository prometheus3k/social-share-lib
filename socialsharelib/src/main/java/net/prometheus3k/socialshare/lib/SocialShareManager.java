/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.socialshare.lib;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.ArrayAdapter;

import net.prometheus3k.socialshare.lib.socialnetworks.FacebookAppIntent;
import net.prometheus3k.socialshare.lib.socialnetworks.GooglePlus;
import net.prometheus3k.socialshare.lib.socialnetworks.TwitterApp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by babumadhikarmi on 09/05/2015.
 * SocialShareManager
 *
 */
public class SocialShareManager {
    private static final String TAG = SocialShareManager.class.getSimpleName();
    private static final boolean DLOG = false;

    //Note: protected so MockSocialShareManager can implement addSocialNetwork() which
    //can be called multiple times after instantiation.
    protected List<SocialNetwork> mSocialNetworkClients = new ArrayList<>();
    private Context mContext;

    //no args constructor: helps in Unit Test MockSocialShareManager
    SocialShareManager() {}

    /**
     *
     * @param activityContext
     */
    public SocialShareManager(Context activityContext) {
        mContext = activityContext;
        //Google+
        mSocialNetworkClients.add(new GooglePlus(mContext));

        //Twitter
        List<TwitterApp> twitterClients = TwitterApp.getClients(mContext);
        if(!twitterClients.isEmpty()){
            //Native Twitter client apps.
            mSocialNetworkClients.addAll(twitterClients);
        }

//      TODO Re enable web based clients once we know how Facebook and G+ can do the same.
//        else if(DBG_ALWAYS_ADD_WEB_OPTION || enableShareViaBrowser){
//            //we fall back to web browsers for Twitter
//            mSocialNetworkClients.addAll(TwitterWeb.getClients(mContext));
//        }

        //Facebook
        //we use a normal facebook app via intent
        mSocialNetworkClients.add(new FacebookAppIntent(mContext));
    }
    
    public Error share(ShareItem shareItem, SocialNetwork.Platform platform){
        //TODO check for internet connection, if none, return with error. http://stackoverflow.com/a/27312494
        return startShare(shareItem, platform);
    }

    private Error startShare(ShareItem shareItem, SocialNetwork.Platform platform){
        //get list of SocialNetworks for network platform
        List<SocialNetwork> compatibleClients = getCompatibleNetworks(shareItem, platform);

        if(DLOG) {
            Log.d(TAG, "compatibleClients " + compatibleClients.size());
            for (SocialNetwork network : compatibleClients) {
                Log.d(TAG, "** " + network.toString());
            }
        }

        if(compatibleClients.size() == 0){
            return new Error("No compatible clients to share with");

        } else if(compatibleClients.size() == 1){
            //trigger the only compatible social network
            compatibleClients.get(0).share(shareItem);

        } else {
            //show a dialog of choice
            showChoiceDialog(compatibleClients, shareItem);
        }

        return null;
    }

    protected List<SocialNetwork> getCompatibleNetworks(ShareItem shareItem, SocialNetwork.Platform platform){
        List<SocialNetwork> compatibleClients = new ArrayList<>();
        for(SocialNetwork client : mSocialNetworkClients){
            if(client.isInstalled() && client.canShare(shareItem)){
                if(platform == SocialNetwork.Platform.Any || client.getNetworkPlatform() == platform){
                    compatibleClients.add(client);
                }
            }
        }
        return compatibleClients;
    }

    private void showChoiceDialog(List<SocialNetwork> clients, final ShareItem shareItem){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getString(R.string.label_share_with));
        final ArrayAdapter<SocialNetwork> adapter = new AppArrayAdapter(mContext, clients);

        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                adapter.getItem(i).share(shareItem);
            }
        });
        builder.show();
    }
}
