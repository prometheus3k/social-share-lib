# README #

### What is this repository for? ###

A simply library to share text, image or text + image to a Social Network (Twitter, Facebook or Google+). This library presents a unified API to share items via these Social Networks and even displays a basic "Share With" dialog, prompting the user to choose the network. You can also specify particular Social Networks to share with (e.g. Facebook only).

**Requirements:**

* Android Studio
* Android API level 14+
* Currently all done via Intents, so no SDK integration required. However the library require the respective social network's Android App i.e. official Facebook app, official Google+ app, one of the supported Twitter clients including official Twitter app. If the user doesn't have the app installed, the Social Network will not appear in the Share With dialog.

**Supported Twitter apps:** 

* Official Twitter app
* EchoFon
* Plume
* Tweedle
* Hootsuite

**Facebook support limitation** 
The Facebook app does not allow passing of string as user's status message (it ignores "EXTRA_TEXT" field in the Intent). This appears to be by design as doing so would breach the [Facebook Platform Policy on Pre-Filling status](https://developers.facebook.com/docs/apps/review/prefill). Whilst technically you can get around this by using the Facebook SDK, that will require Facebook Login review, and so the policy would be enforced (your app would fail the review). 
The closest my library will get you is to copy the status you want to share, to the system wide clipboard, fire up the Facebook App and notify the user they need to long press to paste the status.

I've included a test app in the project.

### How do I get set up? ###
import this library and initialise as follows:
 
    :::java
        SocialManager mSocialShareManager = new SocialShareManager(someContext);

To share some text:
 
    :::java
       ShareItem shareItem = new ShareItem("Hello World!");
       mSocialShareManager.share(shareItem, SocialNetwork.Platform.Any);

SocialShareManager may return and Error object or null. Make sure you handle the case an Error is returned.

### Contribution guidelines ###

* Feel free to make changes/bugfixes, but if you do so, please submit via merge/pull request.

### Who do I talk to? ###

* Me (Babu Madhikarmi) prometheus3k@gmail.com

### Credits ###
Penfold icon source 
[Gedeon Maheux, Findicons.com](http://findicons.com/icon/136056/penfold)


![socialsharelib_pic.png](https://bitbucket.org/repo/G98gRB/images/3052911819-socialsharelib_pic.png)