package net.prometheus3k.socialshare.client;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import net.prometheus3k.socialshare.lib.ShareItem;
import net.prometheus3k.socialshare.lib.SocialNetwork;
import net.prometheus3k.socialshare.lib.SocialShareManager;

public class MainActivityFragment extends Fragment {
    private static final String TAG = MainActivityFragment.class.getSimpleName();

    private SocialShareManager mSocialShareManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //with facebook intent
        mSocialShareManager = new SocialShareManager(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        Button shareTextButton = (Button) view.findViewById(R.id.share_text_button);

        shareTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareItem shareItem = new ShareItem(getText(R.string.share_text).toString());
                handleError(mSocialShareManager.share(shareItem, SocialNetwork.Platform.Any));
            }
        });

        Button shareImageButton = (Button) view.findViewById(R.id.share_image_button);
        Button shareTextImageButton = (Button) view.findViewById(R.id.share_text_image_button);

        if (MainActivity.sTestImagePath != null){
            shareImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShareItem shareItem = new ShareItem(MainActivity.sTestImagePath);
                    handleError(mSocialShareManager.share(shareItem, SocialNetwork.Platform.Any));
                }
            });

            shareTextImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShareItem shareItem = new ShareItem(getText(R.string.share_text).toString(), MainActivity.sTestImagePath);
                    handleError(mSocialShareManager.share(shareItem, SocialNetwork.Platform.Any));
                }
            });
        } else {
            shareImageButton.setEnabled(false);
            shareTextImageButton.setEnabled(false);
        }

        return view;
    }

    private void handleError(Error error){
        if(error != null){
            Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

}
