package net.prometheus3k.socialshare.client;

import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    //TODO hmm, yeah need to sort out a cleaner way for fragment to access this. It'll do for now.
    public static Uri sTestImagePath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debugCopyFile();
        setContentView(R.layout.activity_main);
    }

    private void debugCopyFile(){
        AssetManager assetManager = getResources().getAssets();
        String filename = "penfold_sharable.png";
        InputStream in = null;
        OutputStream out = null;

        try {
            in = assetManager.open(filename);
            File outFile = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), filename);
            Log.w(TAG, "outFile: " + outFile.getAbsolutePath());
            if(outFile.exists()){
                Log.w(TAG, "got outFile");
                sTestImagePath = Uri.fromFile(outFile);
                return;
            }
            Log.w(TAG, "not got outFile, copy from asset folder");
            outFile.getParentFile().mkdirs();

            out = new FileOutputStream(outFile);
            copyFile(in, out);
            sTestImagePath = Uri.fromFile(outFile);
        } catch(IOException e) {
            Log.e(TAG, "Failed to copy asset file: " + filename, e);
        }
        finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // NOOP
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    // NOOP
                }
            }
        }

    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.w(TAG, "onActivityResult() requestCode: " + requestCode + ", resultCode: " + resultCode);
    }
}
